package controllers

import (
	"net/http"

	"gitlab.com/jacky-htg/wira-erp-backend/config"
	"gitlab.com/jacky-htg/wira-erp-backend/libraries"
	"gitlab.com/jacky-htg/wira-erp-backend/models"
	"gitlab.com/jacky-htg/wira-erp-backend/repositories"
)

var userLogin models.User

func checkAuth(w *http.ResponseWriter, req *http.Request, controller string, method string) bool {
	if !checkAuthToken(w, req) {
		return false
	}

	isAuth, err := repositories.AuthCheck(userLogin.Email, controller, method)
	if err != nil {
		http.Error(*w, err.Error(), http.StatusInternalServerError)
		return false
	}

	if !isAuth {
		http.Error(*w, "Forbiden Access", http.StatusForbidden)
		return false
	}

	return true
}

func checkAuthToken(w *http.ResponseWriter, req *http.Request) bool {
	if !checkApiKey(w, req) {
		return false
	}

	if len(req.Header["Token"]) == 0 {
		http.Error(*w, "Please suplay valid token", http.StatusBadRequest)
		return false
	}

	isTokenValid, paramEmail := libraries.ValidateToken(req.Header["Token"][0])

	if !isTokenValid {
		http.Error(*w, "token tidak valid", http.StatusUnauthorized)
		return false
	}

	userLogin, _ = repositories.UserGetByEmail(paramEmail)

	return true
}

func checkApiKey(w *http.ResponseWriter, req *http.Request) bool {
	if len(req.Header["X-Api-Key"]) == 0 {
		http.Error(*w, "Please suplay valid API key", http.StatusBadRequest)
		return false
	}

	if req.Header["X-Api-Key"][0] != config.GetString("auth.apiKey") {
		http.Error(*w, "The API key is invalid", http.StatusUnauthorized)
		return false
	}

	return true
}

func checkError(w http.ResponseWriter, err error) bool {
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		libraries.CheckError(err)
		return false
	}

	return true
}

func checkNull(w http.ResponseWriter, length int, err error) bool {
	if length <= 0 {
		http.Error(w, err.Error(), http.StatusNotFound)
		return false
	}

	return true
}
