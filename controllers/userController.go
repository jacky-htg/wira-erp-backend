package controllers

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/jacky-htg/wira-erp-backend/config"
	"gitlab.com/jacky-htg/wira-erp-backend/libraries"
	"gitlab.com/jacky-htg/wira-erp-backend/models"
	"gitlab.com/jacky-htg/wira-erp-backend/repositories"
)

func UsersGetListHandler(w http.ResponseWriter, req *http.Request) {
	/*if !checkAuthToken(&w, req) {
		return
	}*/

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode([]models.User{})
}

func UsersGetByIdHandler(w http.ResponseWriter, req *http.Request) {
	/*if !checkAuthToken(&w, req) {
		return
	}*/

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(models.User{})
}

func UsersCreateHandler(w http.ResponseWriter, req *http.Request) {
	/*if !checkAuthToken(&w, req) {
		return
	}*/

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(models.User{})
}

func UsersUpdateHandler(w http.ResponseWriter, req *http.Request) {
	/*if !checkAuthToken(&w, req) {
		return
	}*/

	var user models.User

	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(&user)

	if err != nil {
		panic(err)
	}
	defer req.Body.Close()

	if len(user.Photo) != 0 && user.Photo[0:10] == "data:image" {
		i2 := strings.Index(user.Photo, ";")
		if i2 < 0 {
			err = errors.New("Please suplay valid base64 image")
		}

		if !checkError(w, err) {
			return
		}

		h := sha1.New()
		h.Write([]byte(fmt.Sprint(userLogin.ID)))
		bs := h.Sum(nil)
		path := "/users/" + hex.EncodeToString(bs[:]) + "." + user.Photo[11:i2]

		err = libraries.AwsUploadS3(user.Photo, path)
		if !checkError(w, err) {
			return
		}

		user.Photo = config.GetString("aws.s3.url") + path
	}

	// sanitation and validation of user
	user, err = models.UserValidation(user)
	if !checkError(w, err) {
		return
	}

	user, err = repositories.UserEdit(user)
	if !checkError(w, err) {
		return
	}
	user.Password = nil

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)

}
