package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/jacky-htg/wira-erp-backend/libraries"
	"gitlab.com/jacky-htg/wira-erp-backend/repositories"

	"golang.org/x/crypto/bcrypt"
)

type authLogin struct {
	Email    string
	Password string
}

func AuthGetTokenHandler(w http.ResponseWriter, req *http.Request) {
	if !checkApiKey(&w, req) {
		return
	}

	var login authLogin

	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(&login)

	if err != nil {
		panic(err)
	}
	defer req.Body.Close()

	if login.Email == "" || login.Password == "" {
		http.Error(w, "Invalid email or password", http.StatusBadRequest)
		return
	}

	userToken, err := repositories.UserGetByEmail(login.Email)
	if !checkError(w, err) {
		return
	}

	if userToken.ID <= 0 {
		http.Error(w, "user not found", http.StatusNotFound)
		return
	}

	if !userToken.IsActive {
		http.Error(w, "user inactivated", http.StatusNotFound)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(userToken.Password), []byte(login.Password))
	if !checkError(w, err) {
		return
	}

	tokenString, err := libraries.ClaimToken(login.Email)
	if !checkError(w, err) {
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(tokenString)
}
