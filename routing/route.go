package routing

import (
	"net/http"

	"gitlab.com/jacky-htg/wira-erp-backend/controllers"

	"github.com/gorilla/mux"
)

type Route struct {
	Path    string
	Method  string
	Handler http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{"/get-token", "POST", controllers.AuthGetTokenHandler},
	Route{"/users", "GET", controllers.UsersGetListHandler},
	Route{"/users/{id}", "GET", controllers.UsersGetByIdHandler},
	Route{"/users", "POST", controllers.UsersCreateHandler},
	Route{"/users/{id}", "PUT", controllers.UsersUpdateHandler},
}

func NewRouter() *mux.Router {
	router := mux.NewRouter()

	for _, route := range routes {
		router.HandleFunc(route.Path, route.Handler).Methods(route.Method)
	}

	router.
		PathPrefix("/documentation/").
		Handler(http.StripPrefix("/documentation/", http.FileServer(http.Dir("./documentation"))))

	return router
}
