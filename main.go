package main

import (
	"log"
	"net/http"

	"gitlab.com/jacky-htg/wira-erp-backend/config"
	"gitlab.com/jacky-htg/wira-erp-backend/routing"

	"github.com/gorilla/handlers"
)

func main() {
	router := routing.NewRouter()
	allowedHeaders := handlers.AllowedHeaders([]string{"X-Api-Key"})
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	err := http.ListenAndServe(config.GetString("server.address"), handlers.CORS(allowedHeaders, allowedOrigins, allowedMethods)(router))

	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
