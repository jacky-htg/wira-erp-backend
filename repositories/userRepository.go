package repositories

import (
	"database/sql"
	"time"

	"gitlab.com/jacky-htg/wira-erp-backend/libraries"
	"gitlab.com/jacky-htg/wira-erp-backend/models"
)

func UserGetByEmail(paramEmail string) (models.User, error) {
	return userGetRow(db.Query("SELECT `id`, `name`, `email`, `password`, `group_id`, `is_active`, `phone_number`, `photo`, `birthdate`, `gender`, `created`, `updated`  FROM `users` WHERE `email`=?", paramEmail))
}

func UserGet(paramId uint) (models.User, error) {
	return userGetRow(db.Query("SELECT `id`, `name`, `email`, `password`, `group_id`, `is_active`, `phone_number`, `photo`, `birthdate`, `gender`, `city_id`, `created`, `updated`  FROM `users` WHERE `id`=?", paramId))
}

func UserCreate(user models.User) (models.User, error) {
	stmt, err := db.Prepare("INSERT INTO `users` (`name`, `email`, `password`, `group_id`, `created`, `updated`) VALUES (?, ?, ?, ?, NOW(), NOW())")
	libraries.CheckError(err)

	if err != nil {
		return models.User{}, err
	}

	res, err := stmt.Exec(user.Name, user.Email, user.Password, user.Group.ID)
	libraries.CheckError(err)

	if err != nil {
		return models.User{}, err
	}

	id, err := res.LastInsertId()
	libraries.CheckError(err)

	if err != nil {
		return models.User{}, err
	}

	return UserGet(uint(id))
}

func UserChangePassword(user models.User) (models.User, error) {
	stmt, err := db.Prepare("UPDATE `users` SET `password`=?, `updated_at`=NOW() WHERE `id`=?")
	libraries.CheckError(err)

	if err != nil {
		return models.User{}, err
	}

	_, err = stmt.Exec(user.Password, user.ID)
	libraries.CheckError(err)

	if err != nil {
		return models.User{}, err
	}

	user.Password = nil

	return user, err
}

func UserEdit(user models.User) (models.User, error) {
	stmt, err := db.Prepare("UPDATE `users` SET `name`=?, `phone_number`=?, `photo`=?, `birthdate`=?, `gender`=?, `updated`=NOW() WHERE `id`=?")
	libraries.CheckError(err)

	if err != nil {
		return models.User{}, err
	}

	if user.Birthdate.Format(time.RFC822) == "01 Jan 01 00:00 UTC" {
		_, err = stmt.Exec(user.Name, user.PhoneNumber, user.Photo, nil, user.Gender, user.ID)
	} else {
		_, err = stmt.Exec(user.Name, user.PhoneNumber, user.Photo, user.Birthdate, user.Gender, user.ID)
	}

	libraries.CheckError(err)

	if err != nil {
		return models.User{}, err
	}

	return UserGet(user.ID)
}

func userGetRow(rows *sql.Rows, err error) (models.User, error) {
	var user models.User
	libraries.CheckError(err)

	if err != nil {
		return models.User{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var userNull models.UserNull
		err := rows.Scan(&user.ID, &user.Name, &user.Email, &user.Password, &user.Group.ID, &user.IsActive, &userNull.PhoneNumber, &userNull.Photo, &userNull.Birthdate, &user.Gender, &user.CreatedAt, &user.UpdatedAt)
		libraries.CheckError(err)

		if err != nil {
			return models.User{}, err
		}

		user.PhoneNumber = userNull.PhoneNumber.String
		user.Photo = userNull.Photo.String
		user.Birthdate = userNull.Birthdate.Time
	}

	err = rows.Err()
	libraries.CheckError(err)

	if err != nil {
		return models.User{}, err
	}

	/*user.Group, err = GroupGet(user.Group.ID)
	if err != nil {
		return models.User{}, err
	}*/

	return user, err
}
