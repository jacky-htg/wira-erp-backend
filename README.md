# README #

Wira ERP API using GO Language and MySql

### What is this repository for? ###

* API for Wira ERP
* 1.0
* [Learn Markdown]

### How do I get set up? ###

This API using Golang. Go is an open source programming language that makes it easy to build simple, reliable, and efficient software. 

to install go, https://golang.org/doc/install

#### Dependencies : ####
* go get github.com/spf13/viper
* go get github.com/gorilla/handlers
* go get github.com/gorilla/mux
* go get golang.org/x/crypto/bcrypt
* go get github.com/dgrijalva/jwt-go
* go get github.com/go-sql-driver/mysql
* go get gopkg.in/redis.v5

File Configuration on ./config/config.json

You can run application with going the directory wira-erp-backend and command : go run main.go

For api documentation, you can open on the browser: http://localhost:8080/documentation/